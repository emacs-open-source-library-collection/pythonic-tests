#+TITLE: Pythonic tests

* Introduction

This library makes 3 functions available to run your python tests it uses the awesome pythonic library so that tests will run inside containers and virtual environments.

See [[https://github.com/proofit404/pythonic][Pythonic]] to for the library that abstracts this code to work with virtual env's and containers and other environments.

Its currently designed to work with pytest only but it may be easy to modify for other testing libraries.

* Available functions
Bind the below functions to which ever key combination you fancy.

Run all tests in the current project
#+BEGIN_SRC emacs-lisp
(pythonic-tests-all)
#+END_SRC

Run all tests with in the current buffer.
#+BEGIN_SRC emacs-lisp
(pythonic-tests-buffer)
#+END_SRC

Run the test under the cursor,  searches upwards from current cursor point.
#+BEGIN_SRC emacs-lisp
(pythonic-tests-run)
#+END_SRC

** Configuration

Override project parameters using .dir-locals with something like the following.
#+BEGIN_SRC emacs-lisp
((nil . ((pythonic-pytests-params . (list "-s" "-v" "--reuse-db")))))
#+END_SRC

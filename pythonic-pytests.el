;;; pythonic-pytests.el --- Run python tests with pythonic -*- lexical-binding: t; -*-

;; Copyright (C) 2019  Oliver Marks

;; Author: Oliver Marks <oly@digitaloctave.com>
;; URL: https://gitlab.com/emacs-open-source-library-collection/pythonic-pytests
;; Keywords: Tests tools
;; Version: 0.1
;; Created 10 June 2019
;; Package-Requires: ((emacs "24.4"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implid warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Helper for pytest to run your tests from with in Emacs,
;; it will use pythonic and thus use your local host, virtualenv or docker container.

;;; Code:

;;(defvar pythonic-pytests-params " --reuse-db ")
(defvar pythonic-pytests-params '("-s"))


(defun pythonic-pytests-append-dashes ()
  "IPython handles module parameters differently so append the --."
  (message "pytest %s" pythonic-docker-compose-service-name)
  (if (string-match-p (regexp-quote "ipython") python-shell-interpreter) "--"))


(defun pythonic-pytests-project-root ()
  (or (locate-dominating-file default-directory ".git")
      (locate-dominating-file default-directory "setup.py")
      (locate-dominating-file default-directory ".projectile")
      (locate-dominating-file default-directory ".dir-locals")
      (error "Can't find project root file.")))


(defun pythonic-pytests-current ()
  "Find test name currently under point."
  (beginning-of-defun)
  (python-info-current-defun))


(defun pythonic-pytests-list ()
  "Get a list of test's in the current buffer."
  (goto-char (point-min))
  (python-nav-forward-defun)
  (beginning-of-defun)
  (python-info-current-defun))

;;###autoload
(defun pythonic-pytests-all ()
  "Run all test's in project root."
  (interactive)
  (pythonic-pytests))

;;###autoload
(defun pythonic-pytests-buffer ()
  "Run all test's in current buffer."
  (interactive)
  (let* ((arguments (append
                     (list (pythonic-pytests-append-dashes))
                     pythonic-pytests-params
                     (list "-k" (file-name-nondirectory buffer-file-name)))))
    (apply 'pythonic-pytests (remove nil arguments))))



;;###autoload
(defun pythonic-pytests-run ()
  "Run test under point or all test's if nothing under point."
  (interactive)
  (let* ((current-test (pythonic-pytests-current))
         (arguments (append
                     (list (pythonic-pytests-append-dashes))
                     pythonic-pytests-params
                     (list "-k" current-test))))
    (if current-test (apply 'pythonic-pytests (remove nil arguments))
      (pythonic-pytests))))

(defun pythonic-pytests
    (&rest
     args)
  "Run management COMMAND in the comint buffer."
  (let* ((buffer (get-buffer-create "*Pytest*"))
         (process (get-buffer-process buffer))
         (arguments (append (list "-m" "pytest") args))
         (project-root (pythonic-pytests-project-root)))
    (when (and process
               (process-live-p process))
      (setq buffer (generate-new-buffer "*Pytest*")))
    (with-current-buffer buffer (hack-dir-local-variables-non-file-buffer)
                         (let ((inhibit-read-only t))
                           (erase-buffer))
                         (insert (format "Running tests in %s with %s\n"
                                         project-root
                                         (mapconcat 'identity arguments " ")))
                         (insert (mapconcat 'identity arguments " "))
                         (insert "\n")
                         (pythonic-start-process :process "pytest"
                                                 :buffer buffer
                                                 :args arguments
                                                 :cwd project-root
                                                 :filter (lambda (process string)
                                                           (comint-output-filter process
                                                                                 (ansi-color-apply
                                                                                  string))))
                         (move-marker (process-mark (get-buffer-process buffer)) (point-max) buffer)

                         (comint-mode)
                         (setq-local comint-prompt-read-only t)
                         (pop-to-buffer buffer))))

(provide 'pythonic-pytests)
;;; pythonic-pytests.el ends here
